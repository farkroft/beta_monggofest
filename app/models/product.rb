class Product < ApplicationRecord
    belongs_to :product_type
    belongs_to :province
    has_one :product_invest
end
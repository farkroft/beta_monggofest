# class controller for models product_invest
class Api::V1::ProductInvestsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    prod_invests = ProductInvest.all
    if prod_invests.present?
      render json: prod_invests, status: :ok
    else
      render json: { status: 'FAIL',
                     results: nil, errors: 'product invests not found' },
             status: :unprocesable_entity
    end
  end

  def show
    prod_invest = ProductInvest.find(params[:id])
    render json: { status: 'OK',
                   results: prod_invest, errors: nil }, status: :ok
  end

  def create
    prod_invest = ProductInvest.new(prod_invest_params)
    if prod_invest.save
      render json: { status: 'OK',
                     results: prod_invest, errors: nil }, status: :created
    else
      render json: { status: 'FAIL', results: nil,
                     errors: 'product invest not created' },
             status: :unprocesable_entity
    end
  end

  def destroy
    @prod_invest = ProductInvest.find(params[:id])
    # @prod_invest
    if @prod_invest.destroy
        render json: {
          status: 'OK',
          results: 'Product Invest has been deleted', errors: nil
        }, status: :ok
    else
      render json: { status: 'FAIL', results: nil, errors: 'Delete fail' }
    end
  end

  def update
    @prod_invest = ProductInvest.find(params[:id])
    update_prod_invest = @prod_invest.update(prod_invest_params)
    if update_prod_invest
      render json: {
        status: 'OK', results: update_prod_invest, errors: nil
      }, status: :ok
    else
      render json: {
        status: 'FAIL', results: nil, errors: 'Product Invest fail to update'
      }, status: :unprocesable_entity
    end
  end

private

  def find_prod_invest
    @prodinvest = ProductInvest.find(params[:id])
    # methodnya error, variable dipanggil dilain method
  end

  def prod_invest_params
    params.permit(:product_id, :price, :slot, :count_view)
    # jika ingin menggunakan params.require(:prod_invest).permit(:product_id, :price, :slot, :count_view),
    # maka JSON yang dilempar harus didalam object prod_invest{isi value yang ingin di tambah}
    # jika tidak maka JSON yang dilempar tidak perlu didalam object prod_invest 
  end
end

Rails.application.routes.draw do
  # product_invest api
  namespace :api do
    namespace :v1 do
      get 'product_invests', to: 'product_invests#index'
      get 'product_invests/:id', to: 'product_invests#show'
      post 'product_invests', to: 'product_invests#create'
      put 'product_invests/:id', to: 'product_invests#update'
      delete 'product_invests/:id', to: 'product_invests#destroy'
    end
  end
end
